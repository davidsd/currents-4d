#!/bin/bash

module load gcc/12.1.0
module load python/3.9.5
module load Eigen
export PATH="/home/manenti/cft_bootstrap/usr/bin/:$PATH"
ulimit -c unlimited
echo blocks_4d $@

blocks_4d $@
