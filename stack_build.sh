#!/usr/bin/bash
##SBATCH --chdir /work/ext-unige
#SBATCH --nodes 1
#SBATCH --ntasks-per-node=36
#SBATCH --mem 0
#SBATCH --time 24:00:00
#SBATCH --account ext-unige 



module load gcc/8.4.0 

export SDPBDIR="/work/ext-unige/usr"
export LD_LIBRARY_PATH="/work/ext-unige/usr/lib:$LD_LIBRARY_PATH"
export PATH="/work/ext-unige/usr/bin:$PATH"


stack build
