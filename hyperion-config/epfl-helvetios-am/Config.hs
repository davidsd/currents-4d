-- To use, create a symlink 'app/Config.hs' pointing to this file

{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE OverloadedStrings #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm
-- import Hyperion.Bootstrap.Main (HyperionBootstrapConfig (..))

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "epfl-helvetios-pk" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/scratch/manenti/currents4d/")
  { emailAddr = Just "andrea.manenti@physics.uu.se"
  , sshRunCommand = Just ("ssh", ["-f", "-o", "StrictHostKeyChecking no", "OPENBLAS_NUM_THREADS=1"])
  , defaultSbatchOptions = Slurm.defaultSbatchOptions
    { Slurm.account = Just "ext-unige" }
  }

