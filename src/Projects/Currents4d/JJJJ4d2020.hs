{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot    #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.Currents4d.JJJJ4d2020 where

import qualified Blocks.Blocks4d                             as B4d
import           Bootstrap.Bounds.Spectrum                   (setGap,
                                                              addIsolated,
                                                              setTwistGap,
                                                              unitarySpectrum)
import           Bounds.Currents4d.JJJJ4d                    (JJJJ4d (..))
import qualified Bounds.Currents4d.JJJJ4d                    as JJJJ
import           Config                                      (config)
import           Control.Monad.Catch                         (throwM)
import           Control.Monad.Reader                        (local)
import           Data.Aeson                                  (ToJSON)
import           Data.Binary                                 (Binary)
import           Data.Text                                   (Text)
import           GHC.Generics                                (Generic)
import           GHC.Real                                    ((%))
import           Hyperion
import           Hyperion.Bootstrap.BinarySearch             (BinarySearchConfig (..),
                                                              Bracket (..))
import           Hyperion.Bootstrap.Bound                 (BinarySearchBound (..),
                                                              BoundFileTreatment (..),
                                                              Bound (..),
                                                              BoundFiles (..),
                                                              FileTreatment (..),
                                                              HotStartingRule (..),
                                                              defaultBoundFiles,
                                                              defaultResultBoolClosure,
                                                              keepAllFiles,
                                                              remoteBinarySearchBoundWithFileTreatmentAndHotStarting,
                                                              remoteComputeWithFileTreatment)
import qualified Hyperion.Bootstrap.Params          as Params
import           Hyperion.Bootstrap.Main                     (UnknownProgram (..))
import           Hyperion.Util                               (minute, hour)
import           Projects.Currents4d.Defaults                (defaultBoundConfig)
import           SDPB                                        (Params (..))
import qualified SDPB
import           System.FilePath.Posix                       ((</>))

persistentJJJJ4dBlockDir :: B4d.Block4dParams -> FilePath
persistentJJJJ4dBlockDir B4d.Block4dParams {..} = (dataDir config) </> "blocks" </> "JJJJ4d" </>
  ("nmax-"  ++ show nmax ++
  "_kpo-"   ++ show keptPoleOrder ++
  "_kpo_f-" ++ show keptPoleOrder_final ++
  "_order-" ++ show order ++
  "_ff-"    ++ show floatFormat ++
  "_prec-"  ++ show precision)

mkFilesJJJJ4d :: Bound Int JJJJ4d -> FilePath -> BoundFiles
mkFilesJJJJ4d bound dir = (defaultBoundFiles dir)
  { blockDir = persistentJJJJ4dBlockDir (blockParams (boundKey bound)) }

-- | Importantly, we DO NOT remove the blockDir
treatmentJJJJ4d :: BoundFileTreatment
treatmentJJJJ4d = keepAllFiles
  { sdpDirTreatment  = RemoveFile
  , jsonDirTreatment = RemoveFile
  }

remoteComputeJJJJ4dBound :: Bound Int JJJJ4d -> Cluster SDPB.Output
remoteComputeJJJJ4dBound bound =
  remoteComputeWithFileTreatment treatmentJJJJ4d (mkFilesJJJJ4d bound) bound

data JJJJ4dBinarySearch = JJJJ4dBinarySearch
  { ffbs_bound     :: Bound Int JJJJ4d
  , ffbs_config    :: BinarySearchConfig Rational
  , ffbs_gapSector :: (Int, Int)
  } deriving (Show, Generic, Binary, ToJSON)

remoteJJJJ4dBinarySearch :: JJJJ4dBinarySearch -> Cluster (Bracket Rational)
remoteJJJJ4dBinarySearch ffbs =
  remoteBinarySearchBoundWithFileTreatmentAndHotStarting HotStarting treatmentJJJJ4d (mkFilesJJJJ4d (ffbs_bound ffbs)) $
  MkBinarySearchBound
  { bsBoundClosure      = static mkBound `ptrAp` cPure (ffbs_bound ffbs) `cAp` cPure (ffbs_gapSector ffbs)
  , bsConfig            = ffbs_config ffbs
  , bsResultBoolClosure = defaultResultBoolClosure
  }
  where
    mkBound bound gapSector gap =
      bound { boundKey = (boundKey bound) { spectrum = gappedSpectrum } }
      where
        gappedSpectrum = setGap gapSector gap (spectrum (boundKey bound))

jjjj4dDefaultGaps :: Int -> JJJJ4d
jjjj4dDefaultGaps nmax = JJJJ4d
  { spectrum     = setTwistGap 1e-6 $
                   unitarySpectrum
  , objective    = JJJJ.Feasibility
  , blockParams  = B4d.block4dParamsNmax nmax
  , spins        = Params.spinsNmax nmax
  , numPoles = Nothing
  }

-- Unitarity bound
uniBound :: (Int, Int) -> Rational
uniBound x = case x of
                  (0, p) -> 1 + toInteger p % 2
                  (l, p) -> 2 + toInteger p % 2 + toInteger l % 1

-- Refactored similar bounds in a single function
generalAvsBProgramTFixed :: (Int, Int) -> (Int, Int) -> [Rational] -> Cluster ()
generalAvsBProgramTFixed irrepBisected irrepScanned listOfGaps = 
  local (setJobType (MPIJob 1 36) . setJobTime (3*hour)) $
  mapConcurrently_ (remoteJJJJ4dBinarySearch . search)
  listOfGaps
  where
    nmax = 8
    search theGap = JJJJ4dBinarySearch
      { ffbs_bound = Bound
        { boundKey = (jjjj4dDefaultGaps nmax)
          { spectrum     = addIsolated (2,0) 4 $
                         setGap irrepScanned theGap $
                         setTwistGap 1e-6 $
                         unitarySpectrum
          --, blockParams = (B4d.block4dParamsNmax nmax) { B4d.keptPoleOrder_final = 10, B4d.keptPoleOrder = 80, B4d.order = 80, B4d.precision = 1024 }
          }
        , precision = (B4d.block4dParamsNmax nmax :: B4d.Block4dParams).precision
        , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = irrepBisected
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = uniBound irrepBisected
          , falsePoint = 13
          }
        , threshold  = 1e-4
        , terminateTime = Nothing
        }
      }

-- Refactored similar bounds in a single function
generalAvsBProgram :: (Int, Int) -> (Int, Int) -> [Rational] -> Cluster ()
generalAvsBProgram irrepBisected irrepScanned listOfGaps = 
  local (setJobType (MPIJob 1 36) . setJobTime (3*hour)) $
  mapConcurrently_ (remoteJJJJ4dBinarySearch . search)
  listOfGaps
  where
    nmax = 8
    search theGap = JJJJ4dBinarySearch
      { ffbs_bound = Bound
        { boundKey = (jjjj4dDefaultGaps nmax)
          { spectrum     = setGap irrepScanned theGap $
                         setTwistGap 1e-6 $
                         unitarySpectrum
          --, blockParams = (B4d.block4dParamsNmax nmax) { B4d.keptPoleOrder_final = 10, B4d.keptPoleOrder = 80, B4d.order = 80, B4d.precision = 1024 }
          }
        , precision = (B4d.block4dParamsNmax nmax :: B4d.Block4dParams).precision
        , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = irrepBisected
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = uniBound irrepBisected
          , falsePoint = 13
          }
        , threshold  = 1e-6
        , terminateTime = Nothing
        }
      }

generalTestPogram :: Int -> Cluster ()
generalTestPogram nmaxValue =
  local (setJobType (MPIJob 1 36) . setJobTime (2*hour) . setJobMemory "0") $
  mapConcurrently_ (remoteComputeJJJJ4dBound . bound)
  [ 1 -- Should be allowed
  , 9 -- Should be disallowed
  ]
  where
    nmax = nmaxValue
    bound deltaS = Bound
      { boundKey = (jjjj4dDefaultGaps nmax)
        { spectrum     = setGap (0,0) deltaS $
                         setTwistGap 1e-8 $
                         unitarySpectrum
        , blockParams = (B4d.block4dParamsNmax nmax)
           { B4d.keptPoleOrder = 60
           , B4d.precision = 896
           , B4d.keptPoleOrder_final = 25
           }
        }
      , precision = 896  -- B4d.precision (B4d.block4dParamsNmax nmax :: B4d.Block4dParams)
      , solverParams = (Params.jumpFindingParams nmax) 
        { precision = 1024 
        -- This is temporary, remove it
        , findPrimalFeasible = True
        , findDualFeasible = True
        , detectPrimalFeasibleJump = False
        , detectDualFeasibleJump = False
        , primalErrorThreshold = 1.0e-80
        , dualErrorThreshold = 1.0e-30
        -- until here
        }
      , boundConfig = defaultBoundConfig
      }


generalBisectionTestPogram :: Int -> Int -> Cluster ()
generalBisectionTestPogram nmax spinMax =
  local (setJobType (MPIJob 1 36) . setJobTime (23*hour)) $
  mapConcurrently_ (remoteJJJJ4dBinarySearch . search)
  [ () ]
  where
    search () = JJJJ4dBinarySearch
      { ffbs_bound = Bound
        { boundKey = (jjjj4dDefaultGaps nmax)
          { blockParams = (B4d.block4dParamsNmax nmax)
            { B4d.keptPoleOrder = 60
            , B4d.precision = 896
            , B4d.keptPoleOrder_final = 25
            }
          , spins = [0 .. spinMax]
          }
        , precision = 896  -- B4d.precision (B4d.block4dParamsNmax nmax :: B4d.Block4dParams)
        , solverParams = (Params.jumpFindingParams nmax)
          { precision = 1024 
          -- This is temporary, remove it
          , findPrimalFeasible = True
          , findDualFeasible = True
          , detectPrimalFeasibleJump = False
          , detectDualFeasibleJump = False
          , primalErrorThreshold = 1.0e-80
          , dualErrorThreshold = 1.0e-30
          -- until here
          }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, 0)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 1
          , falsePoint = 10
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram :: Text -> Cluster ()

boundsProgram "JJJJ4ddAllowed_test_uppmax_nmax6" =
  local (setJobType (MPIJob 1 124) . setJobTime (1*hour) . setJobMemory "0") $
  mapConcurrently_ (remoteComputeJJJJ4dBound . bound)
  [ 1 -- Should be allowed
  , 9 -- Should be disallowed
  ]
  where
    nmax = 6
    bound deltaS = Bound
      { boundKey = (jjjj4dDefaultGaps nmax)
        { spectrum     = setGap (0,0) deltaS $
                         setTwistGap 1e-6 $
                         unitarySpectrum
        --, blockParams = (B4d.block4dParamsNmax nmax) { B4d.keptPoleOrder = 12 }
        }
      , precision = (B4d.block4dParamsNmax nmax :: B4d.Block4dParams).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 } --768
      , boundConfig = defaultBoundConfig
      }

boundsProgram "JJJJ4ddAllowed_test_nmax6" = generalTestPogram 6
boundsProgram "JJJJ4ddAllowed_test_nmax8" = generalTestPogram 8
boundsProgram "JJJJ4ddAllowed_test_nmax10" = generalTestPogram 10
boundsProgram "JJJJ4ddAllowed_test_nmax14" = generalTestPogram 14
boundsProgram "JJJJ4ddAllowed_test_nmax18" = generalTestPogram 18
boundsProgram "JJJJ4ddAllowed_test_nmax22" = generalTestPogram 22

boundsProgram "JJJJ4ddAllowed_bisection_test_nmax6" = generalBisectionTestPogram 6 50
boundsProgram "JJJJ4ddAllowed_bisection_test_nmax8" = generalBisectionTestPogram 8 50
boundsProgram "JJJJ4ddAllowed_bisection_test_nmax10_spin70" = generalBisectionTestPogram 10 70
boundsProgram "JJJJ4ddAllowed_bisection_test_nmax14_spin70" = generalBisectionTestPogram 14 70
-- boundsProgram "JJJJ4ddAllowed_bisection_test_nmax18" = generalBisectionTestPogram 18 
-- boundsProgram "JJJJ4ddAllowed_bisection_test_nmax22" = generalBisectionTestPogram 22



boundsProgram "JJJJ4d_binary_search_test_nmax6_better_blocks" =
  local (setJobType (MPIJob 1 28) . setJobTime (30*minute)) $
  mapConcurrently_ (remoteJJJJ4dBinarySearch . search)
  [ () ]
  where
    nmax = 6
    search () = JJJJ4dBinarySearch
      { ffbs_bound = Bound
        { boundKey = (jjjj4dDefaultGaps nmax)
          { -- spectrum     = setGap (0,0) deltaS $
            --             setTwistGap 1e-6 $
            --             unitarySpectrum
           blockParams = (B4d.block4dParamsNmax nmax) { B4d.keptPoleOrder_final = 10, B4d.keptPoleOrder = 80, B4d.order = 80, B4d.precision = 1024 }
          }
        , precision = (B4d.block4dParamsNmax nmax :: B4d.Block4dParams).precision
        , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, 0)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 1
          , falsePoint = 10
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }


boundsProgram "JJJJ4d_binary_search_p4l0_nmax8" =
  local (setJobType (MPIJob 1 28) . setJobTime (120*minute)) $
  mapConcurrently_ (remoteJJJJ4dBinarySearch . search)
  [ () ]
  where
    nmax = 8 
    search () = JJJJ4dBinarySearch
      { ffbs_bound = Bound
        { boundKey = jjjj4dDefaultGaps nmax
        , precision = (B4d.block4dParamsNmax nmax :: B4d.Block4dParams).precision
        , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, 4)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 3 
          , falsePoint = 20 
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "JJJJ4d_binary_search_p4l0vsp0l0_nmax8" = generalAvsBProgram (0,4) (0,0) [1, 1.1 .. 8]

boundsProgram "JJJJ4d_binary_search_p0l0vsp2l1_nmax8" = generalAvsBProgram (0,0) (1,2) [3, 3.1 .. 10.2]

boundsProgram "JJJJ4d_binary_search_p2l1vsp0l0_nmax8" = generalAvsBProgram (1,2) (0,0) [4, 4.1 .. 7.4]

boundsProgram "JJJJ4d_binary_search_p0l0vsp0l2_nmax8" = generalAvsBProgramTFixed (0,0) (2,0) [8, 8.1 .. 11.2]

boundsProgram "JJJJ4d_binary_search_p0l2vsp0l0_nmax8" = generalAvsBProgramTFixed (2,0) (0,0) [1.1, 1.2 .. 10]

boundsProgram p = throwM (UnknownProgram p)
-- jjjj4dDefaultGaps :: Int -> JJJJ4d
-- jjjj4dDefaultGaps nmax = JJJJ4d
--   { spectrum     = setTwistGap 1e-6 $
--                    unitarySpectrum
--   , objective    = JJJJ.Feasibility
--   , blockParams  = B4d.block4dParamsNmax nmax
--   , spins        = Params.spinsNmax nmax
--   }
-- 
-- boundsProgram :: Text -> Cluster ()
-- 
-- boundsProgram "JJJJ4ddAllowed_test_nmax6" =
--   local (setJobType (MPIJob 1 28) . setJobTime (25*minute)) $
--   mapConcurrently_ (remoteComputeJJJJ4dBound . bound)
--   [ 1 -- Should be allowed
--   , 9 -- Should be disallowed
--   ]
--   where
--     nmax = 6
--     bound deltaS = Bound
--       { boundKey = (jjjj4dDefaultGaps nmax)
--         { spectrum     = setGap (0,0) deltaS $
--                          setGap (0,4) 4 $
--                          setTwistGap 1 $
--                          unitarySpectrum
--         --, blockParams = (B4d.block4dParamsNmax nmax) { B4d.keptPoleOrder = 12 }
--         }
--       , precision = B4d.precision (B4d.block4dParamsNmax nmax :: B4d.Block4dParams)
--       , solverParams = (Params.jumpFindingParams nmax) { precision = 1260 }
--       , boundConfig = defaultBoundConfig
--       }
-- 
-- boundsProgram "JJJJ4d_binary_search_test_nmax6" =
--   local (setJobType (MPIJob 1 6) . setJobTime (30*minute)) $
--   mapConcurrently_ (remoteJJJJ4dBinarySearch . search)
--   [ () ]
--   where
--     nmax = 6
--     search () = JJJJ4dBinarySearch
--       { ffbs_bound = Bound
--         { boundKey = jjjj4dDefaultGaps nmax
--         , precision = B4d.precision (B4d.block4dParamsNmax nmax :: B4d.Block4dParams)
--         , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
--         , boundConfig = defaultBoundConfig
--         }
--       , ffbs_gapSector = (0, 0)
--       , ffbs_config = BinarySearchConfig
--         { initialBracket = Bracket
--           { truePoint  = 1
--           , falsePoint = 10
--           }
--         , threshold  = 1e-3
--         , terminateTime = Nothing
--         }
--       }
-- 
-- boundsProgram p = throwM (UnknownProgram p)
