#!/bin/bash

module load gcc/8.4.0 mvapich2/2.3.4

srun --mpi=pmi2 /work/ext-unige/usr/bin/sdpb $@
