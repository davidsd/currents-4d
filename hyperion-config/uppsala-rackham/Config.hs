-- To use, create a symlink 'app/Config.hs' pointing to this file

{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE OverloadedStrings #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm
-- import Hyperion.Bootstrap.Main (HyperionBootstrapConfig (..))

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "uppsala-rackham" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/home/manenti/storage_bootstrap/currents4d")
  { emailAddr = Just "andrea.manenti@physics.uu.se"
  , sshRunCommand = Just ("ssh", ["-f", "-o", "StrictHostKeyChecking no", "OPENBLAS_NUM_THREADS=1"])
  , defaultSbatchOptions = Slurm.defaultSbatchOptions
    { Slurm.account = Just "snic2021-22-862" }
  , databaseDir = "/home/manenti/JJJJ_databases/"
  }

