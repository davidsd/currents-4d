{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Projects.Currents4d.Defaults where

import qualified Config
import           Hyperion                          (HyperionConfig (..))
import           Hyperion.Bootstrap.Bound       (BoundConfig (..))
import           Hyperion.Bootstrap.DelaunaySearch (DelaunayConfig (..),
                                                    ChoiceMethod (..))
import qualified QuadraticNet                      as QN
import           System.FilePath.Posix             ((</>))

defaultBoundConfig :: BoundConfig
defaultBoundConfig = BoundConfig Config.scriptsDir

defaultDelaunayConfig :: Int -> Int -> DelaunayConfig
defaultDelaunayConfig nThreads nSteps = DelaunayConfig
  { choiceMethod        = CandidateSeparation
  , terminateTime       = Nothing
  , nThreads            = nThreads
  , nSteps              = nSteps
  , qdelaunayExecutable = Config.scriptsDir </> "qdelaunay.sh"
  }

defaultQuadraticNetConfig :: QN.QuadraticNetConfig
defaultQuadraticNetConfig = QN.defaultQuadraticNetConfig
  (Config.scriptsDir </> "sdp2input.sh")
  (Config.scriptsDir </> "sdpb.sh")
  (dataDir Config.config </> "quadratic-net")
