module Main where

import           Config                         (config, staticConfig)
import           Hyperion.Bootstrap.Main        (hyperionBootstrapMain,
                                                 tryAllPrograms)
import qualified Projects.Currents4d.JJJJ4d2020

main :: IO ()
main = hyperionBootstrapMain config staticConfig $
  tryAllPrograms
  [ Projects.Currents4d.JJJJ4d2020.boundsProgram
  ]
