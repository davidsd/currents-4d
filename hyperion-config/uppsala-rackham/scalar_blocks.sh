#!/bin/bash

module load gcc/9.3.0
module load python/3.9.5
module load openmpi/3.1.5
module load Eigen
export PATH="/home/manenti/cft_bootstrap/usr/bin/:$PATH"
ulimit -c unlimited
echo scalar_blocks $@
scalar_blocks $@
