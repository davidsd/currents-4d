#!/bin/bash

module load gcc/12.1.0
module load python/3.9.5
module load Eigen
export PATH="/home/manenti/cft_bootstrap/usr/bin/:$PATH"
echo seed_blocks $@
seed_blocks $@
