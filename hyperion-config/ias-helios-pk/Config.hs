-- To use, create a symlink 'app/Config.hs' pointing to this file

{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE OverloadedStrings #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
-- import Hyperion.Bootstrap.Main (HyperionBootstrapConfig (..))

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "ias-helios-pk" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/data/pkravchuk/hyperion-jjjj")
  { emailAddr = Just "petr.kravchuk@kcl.ac.uk"
  , sshRunCommand = Just ("ssh", ["-f", "-o", "StrictHostKeyChecking no"])
  }

