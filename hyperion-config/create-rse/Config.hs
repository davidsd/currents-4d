-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "create-rse" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/scratch_tmp/prj/cftspec/rse/currents-4d")
  { defaultSbatchOptions = Slurm.defaultSbatchOptions
    { Slurm.partition = Just "cftspec_cpu"
    }
  , emailAddr = Just "rajeev.erramilli@kcl.ac.uk" }

staticConfig :: HyperionStaticConfig
staticConfig = defaultHyperionStaticConfig {
    commandTransport = SRun (Just ("srun", ["--overlap", "--nodes=1", "--ntasks=1", "--immediate"])) 
  , hostNameStrategy = useSubnet $ Subnet (255, 255, 0, 0) (10, 211, 0, 0)  
  }
