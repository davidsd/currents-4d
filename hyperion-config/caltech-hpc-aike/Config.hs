-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed          (makeRelativeToProject, strToExp)
import           Hyperion

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "caltech-hpc-aike" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/central/groups/dssimmon/aike/test")
  { emailAddr = Just "aliu7@caltech.edu" }
