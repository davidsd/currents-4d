{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UnicodeSyntax         #-}
{-# LANGUAGE TypeOperators       #-}
{-# OPTIONS_GHC -Wno-deferred-out-of-scope-variables #-}

module Bounds.Currents4d.JJJJ4d where

import           Blocks                                       (BlockFetchContext,
                                                               Coordinate (XT),
                                                               CrossingMat,
                                                               DerivMultiplier (..),
                                                               Derivative (..),
                                                               TaylorCoeff (..),
                                                               Taylors, xOdd,
                                                               xtTaylors,
                                                               xtTaylorsAll,
                                                               yEven)
import qualified Blocks.Blocks4d                              as B4d
import           Blocks.Blocks4d.Collection.JJJJ
import           Blocks.Blocks4d.Collection.JJJJ.Descriptions (JJJJDescriptionName)
import           Blocks.Blocks4d.Util                         (Parity (..),
                                                               parity)
import           Blocks.Delta                                 (Delta (..))
import qualified Bootstrap.Bounds.BootstrapSDP                as BSDP
import qualified Bootstrap.Bounds          as Bounds
import           Bootstrap.Bounds.BoundDirection              (BoundDirection,
                                                               boundDirSign)
import           Bootstrap.Bounds.Crossing.CrossingEquations  (FourPointFunctionTerm,
                                                               OPECoefficient,
                                                               crossingMatrix,
                                                               derivsVec,
                                                               mapBlocks,
                                                               opeCoeffIdentical)
import           Bootstrap.Bounds.Spectrum                    (Spectrum,
                                                               listDeltas)
import           Bootstrap.Build                              (FChain (..), FetchConfig (..), HasForce, SomeBuildChain (..), noDeps)
import           Bootstrap.Math.FreeVect                      (FreeVect, vec,
                                                               (*^))
import           Bootstrap.Math.Linear.Literal                (toV)
import           Bootstrap.Math.VectorSpace                   (Tensor, mapTensor, zero, (*^), (^+^), type (⊗))
import Control.DeepSeq                (NFData)
import           Control.Monad.IO.Class                       (liftIO)
import           Data.Aeson                                   (FromJSON, ToJSON)
import           Data.Binary                                  (Binary)
import           Data.Data                                    (Typeable)
import Data.Maybe                     (maybeToList)
import Data.Proxy                     (Proxy (..))
import Data.Reflection                (Reifies, reflect)
import Data.Tagged                    (Tagged)
import qualified Data.Text                   as  Text
import           Data.Vector                                  (Vector)
import qualified Data.Vector                    as Vector
import           GHC.Generics                                 (Generic)
import           GHC.TypeNats                                 (KnownNat)
import           Hyperion                                     (Dict (..),
                                                               Static (..),
                                                               cPtr)
import           Hyperion.Bootstrap.Bound                     (SDPFetchBuildConfig (..),
                                                               ToSDP (..),
                                                               blockDir)
import           Linear.V                                     (V, reifyVectorNat)
import qualified SDPB

data ExternalOp = J
  deriving (Show, Eq, Ord, Enum, Bounded)

--instance (Reifies s Rational) => HasRep (ExternalOp s) (B3d.ConformalRep Rational) where
--  rep x = B3d.ConformalRep (reflect x) (1/2)

data JJJJ4d = JJJJ4d
  { spectrum    :: Spectrum (Int, Int)
  , objective   :: Objective
  , spins       :: [Int]
  , blockParams :: B4d.Block4dParams
  , numPoles :: Maybe Int
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, Bounds.HasBlockParams B4d.Block4dParams)

data Objective
  = Feasibility
  | StressTensorOPEBoundFixed Rational BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

class ThreePointStructure s i r ri where
  makeStructure :: r -> r -> ri -> i -> s

type JJJJNumCrossing = 5

instance Bounds.HasConstraintConfig JJJJNumCrossing JJJJ4d where
  type Deriv JJJJ4d = TaylorCoeff (Derivative 'XT)
  getConstraintConfig = Bounds.defaultConstraintConfig (Proxy @B4d.Block4dParams) crossingEquations

crossingEquations :: forall a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp) JJJJFourPointFunctional b a
  -> V 5 (Taylors 'XT, FreeVect b a)
crossingEquations g0 = toV
  ( (xtTaylors xOdd yEven,  gS 1)
  , (xtTaylorsAll   yEven,  gS 2 - gT 4)
  , (xtTaylors xOdd yEven,  gS 3)
  , (xtTaylorsAll   yEven,  gS 5 - gT 7)
  , (xtTaylors xOdd yEven,  gS 6)
  )
  where
    gS i = g0 J J J J (JJJJFourPointFunctional i SChannel)
    gT i = g0 J J J J (JJJJFourPointFunctional i TChannel)

jjjj4dDerivsVec :: JJJJ4d -> V 5 (Vector (TaylorCoeff (Derivative 'XT)))
jjjj4dDerivsVec f =
  fmap ($ B4d.nmax (blockParams f)) (derivsVec crossingEquations)

jjjj4dCrossingMat
  :: forall j a k. (Reifies k JJJJ4d , KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp) JJJJThreePointStructure a)
  -> (Tagged k ⊗ CrossingMat j JJJJNumCrossing (B4d.Block4d JJJJBlock)) a
jjjj4dCrossingMat channel =
  Bounds.tagVec $ mapBlocks (B4d.Block4d []) $
  crossingMatrix channel crossingEquations

opeJJ 
  :: (Fractional a, Eq a)
  => JJJJInternalRep
  -> [OPECoefficient ExternalOp JJJJThreePointStructure a]
opeJJ iRep@(JJJJInternalRep l p _) =
  map (opeCoeffIdentical J . vec . JJJJThreePointStructure iRep) $
  case (p, parity l, l) of
    (0, Even, 0) -> [1]
    (0, Even, _) -> [1,2]
    (0, Odd, _) -> [1]
    (2, Even, _) -> [1]
    (2, Odd, _) -> [1]
    (4, Even, _) -> [1]
    _ -> error "Internal representation disallowed"

stressTensorMatFixed
  :: forall a k . (Reifies k JJJJ4d ,Floating a, Eq a)
  => Rational -> (Tagged k ⊗ CrossingMat 1 5 (B4d.Block4d JJJJBlock)) a
stressTensorMatFixed gamma = jjjj4dCrossingMat $
  fmap (opeCoeffIdentical J) $ toV (jjtStruct)
  where
    internalRep = JJJJInternalRep 2 0 (Fixed 4)
    struct i = vec $ JJJJThreePointStructure internalRep i
    jjtStruct = (1-32*g)/(36*pi) *^ struct 1
                 - (1+4*g)/(9*pi) *^ struct 2
    g = fromRational gamma

-- currentMat
--   :: forall a k . (Reifies k JJJJ4d, Fractional a, Eq a)
--   => (Tagged k ⊗ CrossingMat 1 5 (B4d.Block4d JJJJBlock)) a
-- currentMat = p0SpinOddMat (JJJJInternalRep 1 0 (Fixed 3))

-- Why do we provide poles to be cancelled in the blocks?
-- We only supply them in
-- 1. p2 spin 1 at 4
-- 2. p4 spin >0 at spin+4

--stressTensorMat
--  :: forall s a . (Floating a, Eq a, Reifies s Rational)
--  => Tagged s (CrossingMat 1 5 B3d.Block3d a)
--stressTensorMat = ffCrossingMat $
--  fmap (opeCoeffIdentical_ (Psi @s) stressTensorRep) $ toV stressTensorStruct
--  where
--    stressTensorStruct =
--      3/(2*sqrt 2*pi) *^ vec (B3d.SO3StructLabel 1 2)
--      - (sqrt 3*dPsi)/(2*pi) *^ vec (B3d.SO3StructLabel 0 2)
--    stressTensorRep = B3d.ConformalRep (RelativeUnitarity 0) 2
--    dPsi = fromRational (reflect @s Psi)

withNonEmptyVec :: [a] -> (forall n . KnownNat n => V n a -> r) -> Maybe r
withNonEmptyVec [] _  = Nothing
withNonEmptyVec xs go = Just $ reifyVectorNat (Vector.fromList xs) go

unit :: (Reifies k JJJJ4d, Fractional a, Eq a) => (Tagged k `Tensor` CrossingMat 1 JJJJNumCrossing JJJJIdentityBlock) a
unit = Bounds.tagVec $ 
  mapBlocks JJJJIdentityBlock $
  crossingMatrix (toV identityOpe) crossingEquations
  where
    identityOpe o1 o2
      | o1 == o2  = vec ()
      | otherwise = zero

bulk 
  :: forall k a m.
  ( Reifies k JJJJ4d, RealFloat a, NFData a, Binary a, Typeable a, BlockFetchContext (B4d.Block4d JJJJBlock) a m, Applicative m, HasForce m)
  => [Tagged k (SDPB.PositiveConstraint m a)] 
bulk = do
  let key = reflect @k Proxy
  p <- [0, 2, 4]
  l <- case p of
    0 -> key.spins
    2 -> filter (>0) key.spins
    4 -> filter even key.spins
    _ -> error "Absurd"
  (delta, range) <- listDeltas (l, p) key.spectrum
  let 
    ope = opeJJ $ JJJJInternalRep l p delta
    label = Text.pack $ "l=" <> show l <> "_p=" <> show p
  maybeToList $ withNonEmptyVec ope $
    Bounds.constraint range label . jjjj4dCrossingMat

jjjj4dSDP
  :: forall a m. ( Binary a, Typeable a, RealFloat a, NFData a, Applicative m, HasForce m,
    BlockFetchContext (B4d.Block4d (JJJJBlock)) a m
     )
  => JJJJ4d
  -> SDPB.SDP m a
jjjj4dSDP key = Bounds.runTagged key $
  case objective key of
    Feasibility ->
      Bounds.sdp (zero `asTypeOf` unit) unit bulk
    StressTensorOPEBoundFixed gamma dir -> --error "not implemented"
      Bounds.sdp unit norm bulk
      where
        norm = boundDirSign dir *^ (stressTensorMatFixed gamma)

instance SDPFetchBuildConfig JJJJ4d where
  sdpFetchConfig _ _ files =
    liftIO . B4d.readBlockTable (blockDir files) :&: FetchNil
  sdpDepBuildChain _ config files = SomeBuildChain $ B4d.block4dBuildChain config files

instance ToSDP JJJJ4d where
  type SDPFetchKeys JJJJ4d = '[ B4d.BlockTableKey JJJJDescriptionName Int ]
  toSDP = jjjj4dSDP

instance Static (Binary JJJJ4d)              where closureDict = cPtr (static Dict)
instance Static (Show JJJJ4d)                where closureDict = cPtr (static Dict)
instance Static (ToSDP JJJJ4d)               where closureDict = cPtr (static Dict)
instance Static (ToJSON JJJJ4d)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig JJJJ4d) where closureDict = cPtr (static Dict)
-- instance Static (BuildInJob JJJJ4d)          where closureDict = cPtr (static Dict)
